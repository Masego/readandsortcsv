﻿using ReadAndSortCSV.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadAndSortCSV
{
    class Program
    {
        static void Main(string[] args)
        {

            //C:\input.csv
            People people = new People();
            people.ReadFile("input.csv");
            Console.WriteLine("**************************************");
            List<RepeatedName> list = people.Sorted();
            foreach(RepeatedName name in list)
            {
                Console.WriteLine(name.ToString());
            }
            people.WriteRepeatedPeople();
            people.WritePeopleAddresses();
            Console.ReadLine();
        }
    }
}
