﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.IO;

namespace ReadAndSortCSV.BO
{

    public class People
    {
        List<Person> PersonList = null;
        public List<RepeatedName> RepeatedNames = null;
        public People()
        {
            PersonList = new List<Person>();
            RepeatedNames = new List<RepeatedName>();
        }

        public void AddPerson(string name, string Address)
        {

            Person person = new Person();
            string[] Names = name.Split();
            for (int i = 0; i < Names.Count() - 1; i++)
            {
                person.Name += Names[i] + " ";
                CheckRepeatedNames(Names[i]);
            }

            person.Surname = Names[Names.Count() - 1].TrimEnd(',');
            CheckRepeatedNames(person.Surname);
            person.Address = Address;
            PersonList.Add(person);

        }

        public void CheckRepeatedNames(string Name)
        {
            RepeatedName name = RepeatedNames.Where(x => x.Name.Trim().ToUpper().Equals(Name.Trim().ToUpper())).FirstOrDefault<RepeatedName>();

            if (name != null)
            {
                name.Count += 1;
                Console.WriteLine("Repeated name :" + name.ToString());
            }
            else
            {
                name = new RepeatedName();

                name.Name = Name;
                name.Count = 1;
                RepeatedNames.Add(name);
            }

        }

        public List<RepeatedName> Sorted()
        {
            return RepeatedNames.OrderByDescending(x => x.Count).ThenBy(x => x.Name).ToList<RepeatedName>();
        }

        public List<Person> SortedAddress()
        {
            return PersonList.OrderBy(x => x.Address).ToList<Person>();
        }


        public void ReadFile(string path)
        {


            try
            {


                using (TextFieldParser csvParser = new TextFieldParser(path))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { "," });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    // Skip the row with the column names
                    // csvParser.ReadLine();

                    while (!csvParser.EndOfData)
                    {
                        // Read current line fields, pointer moves to the next line.
                        string[] fields = csvParser.ReadFields();
                        string Name = fields[0];
                        string Address = fields[1];
                        AddPerson(Name, Address);
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Check if file path exist or the file is not in use");
            }
        }


        public int PeopleCount
        {
            get { return PersonList.Count(); }
        }

        public void WriteRepeatedPeople()
        {
            List<RepeatedName> list = Sorted();
            //before your loop
            var csv = new StringBuilder();

            foreach (RepeatedName name in list)
            {
                csv.AppendLine(name.ToString());
            }

            try
            {
                File.WriteAllText("output1.csv", csv.ToString());
            }
            catch (Exception)
            {
                Console.WriteLine("Error creating file, Check if path exists");
            }
        }


        public void WritePeopleAddresses()
        {
            List<Person> list = SortedAddress();
            //before your loop
            var csv = new StringBuilder();

            foreach (Person person in list)
            {
                csv.AppendLine(person.Address);
            }

            try
            {
                File.WriteAllText("output2.csv", csv.ToString());
            }
            catch (Exception)
            {
                Console.WriteLine("Error creating file, Check if path exists");
            }
        }
    }
}
