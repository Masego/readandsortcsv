﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadAndSortCSV.BO
{
 public   class RepeatedName
    {

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int _count;
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        public override string ToString()
        {
            return _name + ", " + _count;
        }
    }
}
