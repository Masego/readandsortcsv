﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReadAndSortCSV.BO;
using System.Collections.Generic;

namespace ReadAndSortCSV_Test
{
    [TestClass]
    public class PeopleTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void PeopleAddPersonTest()
        {
            //Arrange
            People people = new People();

            //Act
            people.AddPerson("Masego Mpshe","98 Caron Street");

            //Assert
            Assert.AreEqual(1, people.PeopleCount, "Problem adding Person");

        }

        [TestMethod]
        public void CheckRepeatedNamesTest()
        {
            //Arrange
            People people = new People();

            //Act
            people.AddPerson("Mpshe James Mpshe", "98 Caron Street");

            people.AddPerson("James Ingram", "23 Mitsubisi street");

            RepeatedName rname1 = new RepeatedName();
            rname1.Name = "Mpshe";
            rname1.Count = 2;

            RepeatedName rname2 = new RepeatedName();
            rname2.Name = "James";
            rname2.Count = 2;


            List<RepeatedName> RepeatedNames = people.RepeatedNames;
            if (RepeatedNames != null)
            {
                foreach (RepeatedName test in RepeatedNames)
                {
                    if(test.Name.ToUpper().Equals(rname1.Name.Trim().ToUpper()))
                    {
                        Assert.AreEqual(rname1.Count, test.Count, "Repeated names count is invalid");
                    }

                    if (test.Name.ToUpper().Equals(rname2.Name.Trim().ToUpper()))
                    {
                        Assert.AreEqual(rname1.Count, test.Count, "Repeated names count is invalid");
                    }

                }
            }
            else
            {
                Assert.Fail("Repeated names cannot be null");
            }
        }
    }
}
